package co.redeye.core.scoping;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.filer.S3Utils;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

public class S3Filer {

    private static final Logger logger = LoggerFactory.getLogger(co.redeye.core.service.filer.S3Filer.class);

    private final AmazonS3 s3;

    private FileProcessor fileProcessor;

    private long numberOfFilesProcessed = 0;

    public S3Filer(final Boolean localFlag) {
        s3 = new AmazonS3Client();
        s3.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_2)); // most likely default region for redeye docs???
        fileProcessor = new FileProcessor(localFlag);
    }

    // todo: would be cool if we passed in the function that processes the file as a param.
    public void processS3Directory(final String bucket, final String key, final String batchId) throws DocumentProcessorException {
        ObjectListing listing = s3.listObjects(bucket, key);
        processListing(listing, batchId);
        while (listing.isTruncated()) {
            logger.debug("new chunk");
            listing = s3.listNextBatchOfObjects(listing);
            processListing(listing,
                    batchId);
        }
        fileProcessor.cleanUpConnection();
    }

    private void processListing(final ObjectListing listing, final String batchId) throws DocumentProcessorException {
        for (S3ObjectSummary summary : listing.getObjectSummaries()) {
            ++numberOfFilesProcessed;
            logger.debug(summary.getETag() + " " + numberOfFilesProcessed);
            Path tempFilePath = copyS3toTemp(summary.getBucketName(), summary.getKey());
            fileProcessor.process(batchId, tempFilePath, summary);
            deleteLocalFile(tempFilePath);
        }
    }

    /**
     * Retrieve a document from S3 and store in the local file system.
     * <p>
     * <p>
     * returns a path the temporary file.
     *
     * @return String filePath
     */
    private Path copyS3toTemp(final String bucket, final String key) throws DocumentProcessorException {

        final String path = "/tmp/s3/";
        final String fileName = UUID.randomUUID().toString();
        final Path filePath = Paths.get(path + fileName);

        try {
            S3Object object = s3.getObject(new GetObjectRequest(bucket, key));
            writeS3ObjectToDisk(object, filePath);
            object.close();
        } catch (AmazonS3Exception | IOException e) {
            S3Utils.handleException(e);
        }
        return filePath;
    }

    private void writeS3ObjectToDisk(final S3Object s3Object, final Path filePath) throws DocumentProcessorException {
        try {
            Files.copy(s3Object.getObjectContent(), filePath);
        } catch (IOException e) {
            S3Utils.handleException(e);
        }
    }

    public void deleteLocalFile(final Path filePath) throws DocumentProcessorException {
        try {
            Files.delete(filePath);
        } catch (IOException e) {
            S3Utils.handleException(e);
        }
    }

}
