package co.redeye.core.scoping;

import co.redeye.core.scoping.dao.FileHeaderDao;
import co.redeye.core.scoping.objects.FileHeader;
import co.redeye.core.service.parser.DocumentScrape;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.apache.tika.metadata.Metadata;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by george on 25/09/15.
 */
public class FileProcessor {

    private static final Logger logger = LoggerFactory.getLogger(FileProcessor.class);

    private DocumentScrape scrape;

    private FileHeaderDao fileHeaderDAO;

    private Connection connection;

    FileProcessor(final Boolean localFlag) {
        scrape = new DocumentScrape();
        fileHeaderDAO = new FileHeaderDao(MySqlConnector.getConnection(localFlag));
    }

    public void process(final String batchId, final Path file, final S3ObjectSummary summary) {
        logger.debug("processing: " + file.getFileName());

        final String absolutePath = file.toAbsolutePath().toString();
        final String hashKey = getSha(absolutePath);
        final long fileSize = new File(absolutePath).length();
        final String filePath = absolutePath.
                substring(0, absolutePath.lastIndexOf(File.separator));

        final String fname = summary.getKey().substring(summary.getKey().lastIndexOf('/') + 1);
        final String pathName = summary.getBucketName() + "/" +
                summary.getKey().substring(0, summary.getKey().length() - fname.length());

        /** persist to db
         *  note: setting id to null as crazy placeholder. Using autoinc in db.
         */
        FileHeader fileHeader = null;
        try {
            fileHeader = new FileHeader(null, batchId, Files.size(file), Sha.getSha256(file), pathName, fname);
            fileHeaderDAO.create(fileHeader);
        } catch (Exception e) {
            // This is a bit lame. But if anything goes wrong there is nothing we can do and we just want to keep going.
            // report it and keep going.
            logger.debug(e.getMessage());
            e.printStackTrace();
        }
    }

    private Map<String, String> getAttributes(final Metadata metadata) {
        Map<String, String> attributes = new HashMap<>();
        for (String key : metadata.names()) {
            attributes.put(key, metadata.get(key));
        }
        return attributes;
    }

    private String getSha(final String text) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            md.update(text.getBytes("UTF-8")); // or UTF-16 if needed
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        byte[] digest = md.digest();
        return new String(Hex.encode(digest));
    }

    public void cleanUpConnection() {
        if (connection != null)
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

}
