package co.redeye.core.scoping;

import co.redeye.core.service.DocumentProcessorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by george on 25/09/15.
 * <p>
 * This is a CLI utility that takes an S3 bucket path
 * and walks the directory structure, returning the path
 * to every file and then sha256 it and jdbc it into a local or remote
 * mysql db into the redeye.file_header table.
 * <p>
 * > desc  redeye.file_header
 */
public class S3Loader {

    private static final Logger logger = LoggerFactory.getLogger(S3Loader.class);

    private static final String batchId = UUID.randomUUID().toString();

    private static Boolean localFlag = false;

    static void usage() {
        System.err.println("java S3Loader <bucket> <key> [-l local-db]");
        System.exit(-1);
    }

    public static void main(String[] args) throws IOException, DocumentProcessorException {
        String bucket = null;
        String key = null;

        if (args.length == 2) {
            bucket = args[0];
            key = args[1];
        } else if (args.length == 3) {
            bucket = args[0];
            key = args[1];
            if ("-l".equals(args[2])) {
                localFlag = true;
            } else {
                usage();
            }
        } else {
            usage();
        }

        info(bucket, key);
        (new S3Filer(localFlag)).processS3Directory(bucket, key, batchId);
        info(bucket, key);
    }

    private static void info(final String bucket, final String key) {
        logger.info("Batch ID: " + batchId + " ");
        logger.info("Bucket: " + bucket);
        logger.info("Key: " + key);
    }
}
