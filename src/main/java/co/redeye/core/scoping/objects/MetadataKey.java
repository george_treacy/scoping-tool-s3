package co.redeye.core.scoping.objects;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by george on 25/09/15.
 */
public class MetadataKey {

    private static final Integer MAX_KEY_NAME_LENGTH = 256;

    private Integer id;
    private String keyName;

    public MetadataKey(final Integer id, final String keyName) {
        this.id = id;
        this.keyName = keyName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        if (keyName != null && keyName.length() > MAX_KEY_NAME_LENGTH) {
            this.keyName = keyName.substring(0, 255);
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
