package co.redeye.core.scoping.dao;

import co.redeye.core.scoping.MySqlConnector;
import co.redeye.core.scoping.objects.MetadataKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by george on 25/09/15.
 */
public class MetadataKeyDao {

    private static final Logger logger = LoggerFactory.getLogger(MetadataKeyDao.class);

    private static Connection connection;

    public MetadataKeyDao(final Connection connection) {
        this.connection = connection;
    }

    public void create(final MetadataKey metadataKey) throws SQLException {
        String sql = "insert into metadata_key (key_name) values (?)";

        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, metadataKey.getKeyName());

        if (statement.executeUpdate() == 0) {
            logger.debug("we should have got some rows created...");
        }
    }

    public Integer getNextId() {
        Integer id = null;
        try {
            final String sql = "select max(id) from metadata_key";
            PreparedStatement statement = connection.prepareStatement(sql);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    id = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public Integer getKeyIdByName(final String name) {
        Integer id = null;
        try {
            final String sql = "select id from metadata_key where key_name = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, name);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    id = rs.getInt("id");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }
}
