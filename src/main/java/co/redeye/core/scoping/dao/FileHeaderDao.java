package co.redeye.core.scoping.dao;

import co.redeye.core.scoping.MySqlConnector;
import co.redeye.core.scoping.objects.FileHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by george on 25/09/15.
 */
public class FileHeaderDao {

    private static final Logger logger = LoggerFactory.getLogger(FileHeaderDao.class);

    private Connection connection;

    public FileHeaderDao(final Connection connection) {
        this.connection = connection;
    }

    public void create(final FileHeader fileHeader) throws SQLException {
        String sql = "insert into file_header (batch_id, size_bytes, sha_256, path, fname) values (?, ?, ?, ?, ?)";

        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, fileHeader.getBatchId());
        statement.setString(2, fileHeader.getSizeBytes().toString());
        statement.setString(3, fileHeader.getSha256());
        statement.setString(4, fileHeader.getPath());
        statement.setString(5, fileHeader.getFname());

        if (statement.executeUpdate() == 0) {
            logger.error("Create file header failed.");
        }
    }

    // If each path is max 1kb and we get 1 million max so 1Gig of memory hmm.
    public List<FileHeader> getByBatchId(final String batchId) {
        List<FileHeader> fileHeaders = new ArrayList<>();
        try {
            final String sql = "select * from file_header where batch_id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, batchId);
            try (ResultSet rs = statement.executeQuery();) {
                while (rs.next()) {
                    final String filePath = rs.getString("path");
                    final String fileName = rs.getString("fname");
                    fileHeaders.add(new FileHeader(rs.getInt("id"), rs.getString("batch_id"), rs.getLong("size_bytes"), rs.getString("sha_256"), rs.getString("path"), rs.getString("fname")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fileHeaders;
    }


}
