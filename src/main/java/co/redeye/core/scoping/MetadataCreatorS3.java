package co.redeye.core.scoping;

import co.redeye.core.scoping.dao.FileHeaderDao;
import co.redeye.core.scoping.dao.FileMetadataDao;
import co.redeye.core.scoping.dao.MetadataKeyDao;
import co.redeye.core.scoping.objects.FileHeader;
import co.redeye.core.scoping.objects.FileMetadata;
import co.redeye.core.scoping.objects.MetadataKey;
import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.parser.DocumentScrape;
import org.apache.tika.metadata.Metadata;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by george on 25/09/15.
 * <p>
 * This is a CLI utility that reads rows from local mysql file_header table
 * and extracts metadata from the local file and persists it in mysql.
 * <p>
 * > desc  redeye.file_header, file_metadata, metadata_key.
 * <p>
 * Exception handling note: if anything goes wrong we blow up and stop.
 */
public class MetadataCreatorS3 {

    private static final Logger logger = LoggerFactory.getLogger(FileProcessor.class);

    private static String batchId = null;

    private DocumentScrape scrape;
    private FileHeaderDao fileHeaderDAO;
    private MetadataKeyDao metadataKeyDao;
    private FileMetadataDao fileMetadataDao;

    private static Boolean localFlag = false;

    private Connection connection;

    static void usage() {
        System.err.println("java MetadataCreatorS3 <batch_id> [-l local-db]");
        System.exit(-1);
    }

    public static void main(String[] args)
            throws IOException, SQLException {

        if (args.length == 1) {
            batchId = args[0];
        } else if (args.length == 2) {
            batchId = args[0];
            if ("-l".equals(args[1])) {
                localFlag = true;
            } else {
                usage();
            }
        } else {
            usage();
        }

        MetadataCreatorS3 creator = new MetadataCreatorS3();
        creator.run();
    }

    MetadataCreatorS3() {
        connection = MySqlConnector.getConnection(localFlag);
        fileHeaderDAO = new FileHeaderDao(connection);
        metadataKeyDao = new MetadataKeyDao(connection);
        fileMetadataDao = new FileMetadataDao(connection);
        scrape = new DocumentScrape();
    }

    private void run() throws SQLException {
        List<FileHeader> fileHeaders = fileHeaderDAO.getByBatchId(batchId);

        // todo: use stream api.
        for (FileHeader fileHeader : fileHeaders) {
            process(fileHeader);
        }

        if (connection != null) {
            connection.close();
        }
    }

    private void process(final FileHeader fileHeader) throws SQLException {
        scrape.resetParser();
        final String path = fileHeader.getPath() + "/" + fileHeader.getFname();
        try {
            scrape.tikaAutoParse(path);
        } catch (DocumentProcessorException e) {
            e.printStackTrace();
        }

        final String hashKey = getSha(path);

//        // get the file size.
//        final long fileSize = new File(path).length();
//        logger.debug("File Size Bytes:  " + fileSize);

        // BodyText will be limited to max 100,000 chars by tika exception.
        // If the bodyText is less than 100 chars, it is probably a scan etc...
        // even if it's file type is pdf.
        // todo: maybe just persist the number of chars in the body text in the file_header table or as an attribute in metadata.
//        String bodyText = scrape.getHandler().toString();
//        logger.debug("Body Text: " + bodyText.substring(0, Math.min(bodyText.length(), 100)));
//        if (bodyText.length() == 0) {
//            bodyText = "empty";
//        }

        writeAttributes(scrape.getMetadata(), fileHeader.getId());
    }

    private void writeAttributes(final Metadata metadata, final Integer id) throws SQLException {
        for (String key : metadata.names()) {
            final String value =  metadata.get(key);
            Integer keyId = metadataKeyDao.getKeyIdByName(key);
            if (keyId == null) {
                MetadataKey metadataKey = new MetadataKey(null, key);
                metadataKeyDao.create(metadataKey);
                keyId = metadataKeyDao.getNextId();
            }
            FileMetadata fileMetadata = new FileMetadata(id, keyId, value);
            fileMetadataDao.create(fileMetadata);
        }
    }

    private String getSha(final String text) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            md.update(text.getBytes("UTF-8")); // or UTF-16 if needed
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        byte[] digest = md.digest();
        return new String(Hex.encode(digest));
    }


}
