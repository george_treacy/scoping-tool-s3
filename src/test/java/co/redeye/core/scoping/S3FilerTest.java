package co.redeye.core.scoping;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by george on 29/09/15.
 */
public class S3FilerTest {

    private final String bucket = "georgeit";
    private final String key = "QUU";
    private final String batchId = "3434343";

    @Test
    public void processS3DirectoryTest() throws Exception {
        S3Filer filer = new S3Filer(false);
        filer.processS3Directory(bucket, key, batchId);
    }
}